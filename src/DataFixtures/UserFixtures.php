<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Faker\Factory;

class UserFixtures extends Fixture
{
    public const RANDOM_USER = 'Random_user';

    public function __construct (private readonly UserPasswordHasherInterface $userPasswordHasherInterface)
    {
    }

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create();

        for ($i = 0; $i < 5; $i++) {
            $user = new User();
            $username = $faker->userName;
            $user->setUsername($username);
            $user->setPassword($this->userPasswordHasherInterface->hashPassword($user, $username));
            $user->setFirstname($faker->name);
            $user->setMiddlename($faker->name);
            $user->setLastname($faker->name);

            $manager->persist($user);
            if ($i === 0)   $this->addReference(self::RANDOM_USER, $user);
        }
        $manager->flush();
    }
}
